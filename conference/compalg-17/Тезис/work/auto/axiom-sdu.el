(TeX-add-style-hook "axiom-sdu"
 (lambda ()
    (TeX-add-symbols
     "refen")
    (TeX-run-style-hooks
     "babel"
     "english"
     "russian"
     "setspace"
     "hyperref"
     "mathrsfs"
     "url"
     "euscript"
     "srcltx"
     "latexsym"
     "xy"
     "all"
     "fontenc"
     "T2A"
     "inputenc"
     "cp1251"
     "amscd"
     "amssymb"
     "amsthm"
     "amsfonts"
     "amsmath"
     ""
     "latex2e"
     "art12"
     "article"
     "twoside"
     "12pt"
     "A4")))

