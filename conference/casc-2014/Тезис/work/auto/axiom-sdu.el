(TeX-add-style-hook "axiom-sdu"
 (lambda ()
    (LaTeX-add-bibitems
     "p_6::en"
     "p_25::en"
     "L_lit01"
     "L_lit09::en"
     "L_lit11::en"
     "L_lit10::en"
     "L_lit14::en"
     "L_lit12::en"
     "L_lit13"
     "kulyabov:2014:vestnik-miph:onestep::en"
     "lit_sdu_30::en"
     "kulyabov:2013:conference:mephi::en"
     "lotka"
     "sdu_10"
     "penrose-rindler-1987::en"
     "L_lit04"
     "p_5")
    (LaTeX-add-labels
     "sec:intro"
     "sec:notation"
     "sec:onestep"
     "eq:chemkin"
     "eq:r_i"
     "eq:chemkin2"
     "eq:n^i-notion"
     "eq:s-pm"
     "eq:uu"
     "eq:FP"
     "eq:kFP"
     "eq:langevin"
     "eq:k-langevin"
     "eq:lemma_ito"
     "sec:pp-model"
     "sec:pp-model:det"
     "eq:0_4"
     "eq:0_5"
     "sec:pp-model:stoch"
     "eq:3_0"
     "eq:FP_hzh"
     "eq:pp_FP"
     "eq:pp_langevin"
     "sec:axiom"
     "sec:axiom:compare"
     "sec:axiom:realization"
     "fig:texmacs:pp_FP"
     "sec:numerical"
     "fig:prepre_1_2"
     "fig:prepre_1_2d"
     "fig:prepre_2_2"
     "fig:prepre_2_2d"
     "sec:conclusion")
    (TeX-add-symbols
     '("doi" 1)
     '("url" 1)
     '("natexlab" 1)
     '("crd" 1)
     "setR"
     "setZ"
     "doi")
    (TeX-run-style-hooks
     "setspace"
     "breakurl"
     "mathtools"
     "amssymb"
     "amsmath"
     "latex2e"
     "llncs10"
     "llncs"
     "")))

