(TeX-add-style-hook
 "20sdu"
 (lambda ()
   (LaTeX-add-labels
    "sec:onestep"
    "eq:chemkin"
    "eq:r_i"
    "eq:chemkin2"
    "eq:n^i-notion"
    "eq:s-pm"
    "eq:uu"
    "eq:FP"
    "eq:kFP"
    "eq:langevin"
    "eq:k-langevin"
    "eq:lemma_ito")))

