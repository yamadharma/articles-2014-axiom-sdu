(TeX-add-style-hook "23pp"
 (lambda ()
    (LaTeX-add-labels
     "sec:pp-model"
     "sec:pp-model:det"
     "eq:0_4"
     "eq:0_5"
     "sec:pp-model:stoch"
     "eq:3_0"
     "eq:FP_hzh"
     "eq:pp_FP"
     "eq:pp_langevin")))

