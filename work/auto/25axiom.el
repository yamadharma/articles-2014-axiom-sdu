(TeX-add-style-hook
 "25axiom"
 (lambda ()
   (LaTeX-add-labels
    "sec:axiom"
    "sec:axiom:compare"
    "sec:axiom:realization"
    "fig:texmacs:pp_FP")))

