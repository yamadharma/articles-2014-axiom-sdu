(TeX-add-style-hook
 "40numerical"
 (lambda ()
   (LaTeX-add-labels
    "sec:numerical"
    "fig:prepre_1_2"
    "fig:prepre_1_2d"
    "fig:prepre_2_2"
    "fig:prepre_2_2d")))

