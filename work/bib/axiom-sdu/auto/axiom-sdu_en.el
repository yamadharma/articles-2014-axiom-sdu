(TeX-add-style-hook "axiom-sdu_en"
 (lambda ()
    (LaTeX-add-bibitems
     "L_lit01"
     "L_lit04"
     "L_lit09"
     "L_lit10"
     "L_lit11"
     "L_lit12"
     "L_lit13"
     "L_lit14"
     "p_5"
     "p_6"
     "p_25"
     "lit_sdu_30"
     "lotka"
     "kulyabov:2013:conference:mephi"
     "sdu_10"
     "penrose-rindler-1987"
     "kulyabov:2014:vestnik-miph:onestep")))

