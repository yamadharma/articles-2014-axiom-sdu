(TeX-add-style-hook "20sdu"
 (lambda ()
    (LaTeX-add-labels
     "sec:onestep"
     "eq:chemkin"
     "eq:r_i"
     "eq:chemkin2"
     "eq:n^i-notion"
     "eq:s-pm"
     "eq:uu"
     "eq:FP"
     "eq:kFP"
     "eq:langevin"
     "eq:k-langevin"
     "eq:lemma_ito"
     "sec:pp-model"
     "sec:pp-model:det"
     "eq:0_4"
     "eq:0_5"
     "eq:0_6"
     "sec:pp-model:stoch"
     "eq:3_0"
     "eq:FP_hzh"
     "eq:pp_langevin")))

