#! /usr/bin/env python3
# -*- coding: utf-8 -*-
'''
	Это парсер файлов, полученных в результате работы программы символьных вычислений
	Пока предварительный вариант
'''
import Model as md
# Задаем словарь с дополнительной информацией о модели
# возможно в словарь следует добавить информацию из какого
# файла следует читать коэффициенты матриц 

# Основные характеристики модели
prepre = {
	'model_name' : "Стохастическая модель Хищник-Жертва",
	'variables_list' : ['x','y'],
	'parameters_list' : [ 'alpha', 'beta', 'gamma', 'delta' ]
}
# Значения параметров и начальные значения переменных, для которых следует произвести вычисления
p1 = [ 10.0, 1.5, 8.5, 1.8] # Смерть жертв,
init1 = [ p1[2]/p1[3] + 5.0, p1[0]/p1[1] + 0.1 ]

p2 = [ 10.0, 1.5, 8.5, 0.5] # Смерть хищников
init2 = [ p2[2]/p2[3] + 5.0, p2[0]/p2[1] + 0.1 ]

values = [
	{
		'description' : u"Смерть жертв. ",
		'parameters_values' : p1,
		'init_values' : init1,
		'num_method' : {'t_start' : 0.0, 't_stop' : 25.0, 'h' : 0.001}
	},
	{
		'description' : u"Смерть хищников. ",
		'parameters_values' : p2,
		'init_values' : init2,
		'num_method' : {'t_start' : 0.0, 't_stop' : 25.0, 'h' : 0.001}
	}]

# Список внешних файлов с данными
files = { 
	'matrix' : "matrix.data",
	'namelist' : "parameters.data"
}
# Список графиков, которые надо построить
plots = [
	{ 
        'x' : ['t'],
        'y' : ['x','y'],
        'title' : u"Стохастическая модель «Хищник-Жертва»",
        'legend' : [ u"Число жертв", u"Число хищников" ],
        'xlabel' : u"$t_{n}$",
        'ylabel' : u"$x_{n}$ и $y_{n}$ — численность жертв и хищников",
        'file_name' : u"Эволюция численности_ru_",
        'line_type' : ['-', ':'],
        'file_format' : 'pdf'
	},
	{ 
        'x' : ['t'],
        'y' : ['x','y'],
        'title' : u"Stochastic Predator-Prey model",
        'legend' : [ u"Preys quantity", u"Predators quantity" ],
        'xlabel' : u"$t_{n}$",
        'ylabel' : u"$x_{n}$ и $y_{n}$ — preys and predators quantity",
        'file_name' : u"Эволюция численности_en_",
        'line_type' : ['-', ':'],
        'file_format' : 'pdf'
	},
	{ 
        'x' : ['x'],
        'y' : ['y'],
        'title' : u"Фазовые портреты стохастической модели «Хищник-Жертва»",
        'legend' : [u"Фазовый портрет"],
        'xlabel' : u"$x(t)$ — численность жертв",
        'ylabel' : u"$y(t)$ — численность хищников",
        'file_name' : u"Фазовый портрет_ru_",
        'line_type' : ['-'],
        'file_format' : 'pdf'
	},
	{ 
        'x' : ['x'],
        'y' : ['y'],
        'title' : u"Phase portrait of stochastic Predator-Prey model",
        'legend' : [u"Phase portrait"],
        'xlabel' : u"$x(t)$ — preys quantity",
        'ylabel' : u"$y(t)$ — predators quantity",
        'file_name' : u"Фазовый портрет_en_",
        'line_type' : ['-'],
        'file_format' : 'pdf'
	}
	]
# Функция, строящая графики по списку (файл, из которого берутся данные, 
# указан внутри функции, что плохо и что надо изменить)
def PlotData(plots, description):
	'''Служит для построения графиков по вычисленным данным. 
	Данные находятся в файле data.csv'''
	import pandas as pd
	import matplotlib.pyplot as plt
	data = 'data.csv'
		
	table = pd.read_csv(data)

	# Проходим по всем графикам
	for pl in plots:
		fig = plt.figure(0)
		ax =  fig.add_subplot(1,1,1)
		
		ax.set_title(pl['title'], fontsize=10)
		ax.set_xlabel(pl['xlabel'], fontsize=14)
		ax.set_ylabel(pl['ylabel'], fontsize=14)
		
		for (p, leg, lt) in zip(pl['y'], pl['legend'], pl['line_type']):
			ax.plot(table[pl['x']].values, table[p].values, linestyle=lt, color='k', label = leg)
			ax.legend()

		fig.savefig(description + pl['file_name'] + '.' + pl['file_format'] , dpi = 400, format = pl['file_format'], pad_inches=0.1)
		fig.clf()
# Основные действия
prpr = md.Model(prepre)
MT = open(files['matrix'], 'r')
prpr.GenerateSrcFiles(MT)
MT.close()
prpr.SrcCompile()
for value in values:
	print("Calculating and plotting")
	NL = open(files['namelist'], 'w')
	prpr.GenerateNameListFile(value, NL)
	NL.close()
	prpr.ModelCalc()
	PlotData(plots, value['description'])
	



