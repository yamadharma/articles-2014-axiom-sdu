#! /usr/bin/env python3
# -*- coding: utf-8 -*-
''' Модуль для одного класса'''
from templates import templates
import subprocess
class Model(object):
    '''Описывает математическую модель. для которой генерируется
    фортран программа по шаблону. Также содержит метод для генерации
    специфического для фортрана namelist файла, что позволяет менять
    параметры и начальные значения функции.'''
    def __init__(self, model_dic):
        '''Инициализация модели. При инициализации передаются следующие словари:
        - model_dic — основные параметры и свойства модели'''
        self.name = model_dic['model_name']
        self.var_list = model_dic['variables_list']
        self.var_num = len(model_dic['variables_list'])
        self.param_list = model_dic['parameters_list']
        #---
        self.__src_generated = False
        self.__src_compiled = False
        self.__data_calculated = False
        self.__csv_file = 'data.csv'
        # Словарь того, что генерируется
        self.__gen_src = {
                'functions':'./src/functions.f90',
                'main':'./src/main.f90'
                }
        # Список всего, что должно быть
        self.__static_src = [
                             'matrix.f90',
                             'random.f90',
                             'functions.f90',
                             'sde_num_methods.f90',
                             'main.f90'
                            ]
    #---
    def __ParseMatrix(self, file_to_parse):
        '''Внутренний метод для парсинга файла, содержащего элементы матриц
        vector_a и matrix_b стохастическо диф. уравнения.
        file_to_parse — файл, который необходимо распарсить
        '''
        # В списки vector_a и matrix_B записываются
        # компоненты вектора a и матрицы B
        vector_a = []
        matrix_b = []
        # В function_a и function_b записываются
        # готовые тексты функций на языке фортран
        function_a = ""
        function_b = ""
        param_init = ""
        #----
        i = 0
        for line in file_to_parse:
            # Необходимо убрать пробелы. Для этого
            # сначала разбиваем строку на слова по пробелам
            # затем сливаем все в одну строку без пробелов
            line = "".join(line.split())

            # Пропускаем комментарии, разделяющие матрицы
            if line.find('#') == 0:
                i += 1
                continue
            # Для первой матрицы (вектора) строим одномерный список
            if i == 1:
                vector_a.append(line)
            # Для второй матрицы строим двумерный список
            elif i == 2:
                matrix_b.append(line.split(','))

        # Формируем вывод функции Afunc
        i = 1
        for component in vector_a:
            function_a += "Afunc({0}) = {1}\n".format(i, component)
            i += 1

        # Формируем вывод функции Bfunc
        i = 1
        lim = "1:"+str(len(matrix_b))
        for component in matrix_b:
            function_b += "B({0},{1}) = [ {2} ]\n".format(i, lim, ", ".join(component))
            i += 1

        # Теперь разбираемся с параметрами
        i = 1
        for param in self.param_list:
            param_init += "{0} = p({1})\n".format(param, i)
            i += 1

        # Наконец формируем окончательный вид функций
        return (
                templates['Afunc'].format(
                    parameters_list=", ".join(self.param_list),
                    parameters_init=param_init,
                    function_return=function_a
                    ),
                templates['Bfunc'].format(
                    parameters_list=", ".join(self.param_list),
                    parameters_init=param_init,
                    function_return=function_b
                    )
                )

    def GenerateSrcFiles(self, file_to_parse):
        ''' Метод создает два исходных файла с кодом на фортране
        метод возвращает объект
        '''
        file_1 = open(self.__gen_src['functions'], 'w')
        file_2 = open(self.__gen_src['main'], 'w')

        function_a, function_b = self.__ParseMatrix(file_to_parse)

        file_1.write(templates['functions'].format(
                functionA=function_a,
                functionB=function_b
                ))
        file_2.write(templates['main'].format(
                model_name=self.name,
                equations_num=self.var_num,
                parameters_num=len(self.param_list),
                parameters_list=", ".join(self.param_list)
                ))
        file_1.close()
        file_2.close()

        # Исходные коды сгенерированы
        self.__src_generated = True

        return self

    def __GenerateNameListString(self, nl_dic):
        '''
        Метод возвращает строку в виде namelist файла, сформированного
        с помощью переданного ему словаря переменных. Так можно изменять
        параметры программы без перекомпиляции
        '''
        # Список параметров численного метода
        list_1 = ",\n".join(["\t" + l + " = " + str(nl_dic['num_method'][l]) for l in nl_dic['num_method']])
        # Список параметров математической модели
        list_2 = ",\n".join(["\t" + str(pn) + " = " + str(pv) for pn, pv in  zip(self.param_list, nl_dic['parameters_values'])])
        # Список начальных значений
        list_3 = "\t" + "yo=" + ",".join(str(nl_dic['init_values']).strip('[]').split(', '))
        # Необходимо заметить, что в файле именованные списки должны располагаться
        # в том же порядке в каком они будут считываться фортран программой
        return "&num_method_parameters\n{0}\n/\n&model_parameters\n{1},\n{2}\n/\n".format(list_1, list_2, list_3)

    def GenerateNameListFile(self, nl_dic, nl_file):
        '''Метод записывает сгенерированную локальным методом
        __GenerateNameListString() строку в файл. Следует заметить,
				что пока файл не закрыт, Фортран программа не может его считать,
				так что сразу после создания файл необходимо закрыть.'''
				# Возможно это следует сделать в данном методе?
        nl_file.write(self.__GenerateNameListString(nl_dic))
        return self

    def SrcCompile(self):
        '''Компилирует программу. Необходимо привести в божеский вид.
           Сейчас из-за создание o файлов происходит в произвольном
           порядке, так что с первого раза линкуется неправильно.'''
        prog = FortranProg('./src', self.__static_src, 'main')
        prog.SrcCompile().MakeLink('lapack')
        self.__src_compiled = True
        return self

    def ModelCalc(self):
        ''' Запуск вычислений
        '''
        if self.__src_compiled == False:
            self.SrcCompile()

        csv = open(self.__csv_file, 'w')
        proc = subprocess.Popen(['./main'], stdout=subprocess.PIPE)
        output = proc.communicate()[0].decode("utf-8")

        # Первой строкой добавляем названия колонок
        csv.write(",".join(['t'] + self.var_list + self.param_list)+'\n')

        for line in output.splitlines():
            csv.write(",".join(line.split()) + '\n')

        csv.close()

        self.__data_calculated = True

        return self

    def Plot(self):
        ''' Рисуем по одному словарю
        '''
        pass
        return self
#-------------------------------------------------------------------------------
#
#-------------------------------------------------------------------------------
'''Класс реализует упрощенную сборку Фортран программы из исходных текстов'''
class FortranProg(object):
    def __init__(self, src_dir, src_list, main_prog):
        '''Инициализация'''
        self.src_dir = src_dir.rstrip('/') + '/'
        self.src_list = src_list
        self.main_prog = main_prog
        
        self.obj_dir = "./modules".rstrip('/') + '/'
        self.fc = 'gfortran'
        self.fcflags = ['-fcheck=bounds', '-c', '-w']
        self.llflags = ['-cpp']

    def SrcCompile(self):
        '''Компиляция исходных файлов'''
        # названия файлов с путями до них
        f_files = [self.src_dir + f_file for f_file in self.src_list]
        # Названия объектных файлов без путей
        o_files = [f_file.replace('.f90', '.o') for f_file in self.src_list]
        # Названия объектных файлов с путями
        self.o_files = [self.obj_dir + o_file for o_file in o_files]
        
        # print(str(self.o_files))
        # print(str(f_files))
        
        for (f_file, o_file) in zip(f_files, self.o_files):
            cmd = [ self.fc ] + self.fcflags + [f_file, '-o', o_file, '-J' + self.obj_dir]
            print(str(" ".join(cmd)))
            subprocess.call(cmd)
        return self
    
    def __GetLibsFlags(self, lib):
        '''С помощью pkg-config получаем флаги для линковки библиотек'''
        proc = subprocess.Popen(['pkg-config', '--libs', lib], stdout=subprocess.PIPE)
        output = proc.communicate()[0].decode("utf-8")
        return output.split()
    
    def MakeLink(self, lib):
        '''Собираем исполняемый файл'''
        if not hasattr(self, 'o_files'):
            self.SrcCompile(self)
        libflags = self.__GetLibsFlags(lib)
        # gfotran -llflags --libflags <список объектных файлов> -o <название программы>
        cmd = [ self.fc ] + self.llflags + libflags
        cmd += self.o_files + ['-o', self.main_prog]
        print(str(" ".join(cmd)))
        subprocess.call(cmd)
        return 0
    













