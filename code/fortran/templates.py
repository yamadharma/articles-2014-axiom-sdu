# -*- coding: utf-8 -*-
# {functionA}
# {functionB}
Tf = '''
module functions
contains
!------------------------------------------------------------------
!	проверка результатов вычисления на осмысленность
!------------------------------------------------------------------
pure function check(x)
	implicit none
	real(kind = 8), dimension(:), intent(in) :: x
	logical :: check
	if( minval(x) > 0.0d0 ) then
		check = .True.
	else
		check = .False.
	end if
end function check
! Вектор a
{functionA}
! Вектор B
{functionB}
end module functions
'''
# {parameters_list}
# {parameters_init}
# {function_return}
TA = '''
!-----------------------------------
function Afunc(t,x,p)
implicit none
real(kind = 8), intent(in) :: t
real(kind = 8), dimension(:), intent(in) :: x
real(kind = 8), dimension(:), intent(in) :: p ! Параметры
real(kind = 8), dimension(1:ubound(x,1)) :: Afunc
real(kind = 8) :: {parameters_list}

! Инициализируем параметры
{parameters_init}
! Afunc(i) = 
{function_return}
end function Afunc
'''
# {parameters_list}
# {parameters_init}
# {function_return}
TB = '''
!-----------------------------------
function Bfunc(t,x,p)
use matrix
implicit none
real(kind = 8), intent(in) :: t
real(kind = 8), dimension(:), intent(in) :: x
real(kind = 8), dimension(:), intent(in) :: p ! Параметры
real(kind = 8), dimension(lbound(x,1):ubound(x,1),lbound(x,1):ubound(x,1)) :: Bfunc
real(kind = 8) :: {parameters_list}
real(kind = 8), dimension(1:2,1:2) :: B

! p(i) = коэффициент(i) для всех i
{parameters_init}

! Bfunc(i) = 
{function_return}

Bfunc = matrix_sqrt(B,2,2)
end function Bfunc
'''
#
# {model_name} — название модели
# {equations_num} — число уравнений в модели
# {parameters_num} — число параметров модели
# {parameters_list} — список параметров
#
Tm = '''
program main
	use functions
	use sde_num_methods
	implicit none
	! {model_name}
	! eqn — число уравнений
	! h — шаг численного метода
	! t — время
	! y — сеточная функция
	! yo — начальные значения
	! t_start, t_stop — отрезок интегрирования
	! pp — массив параметров функции
	integer, parameter :: eqn = {equations_num}
	real(kind = 8) :: h
	real(kind = 8) :: t
	real(kind = 8), dimension(eqn) :: y
	real(kind = 8), dimension(eqn) :: yo
	real(kind = 8) :: t_start, t_stop
	real(kind = 8), dimension(1:{parameters_num}) :: pp
	real(kind = 8) :: {parameters_list}
	
	namelist /NUM_METHOD_PARAMETERS/ t_start, t_stop, h
	namelist /MODEL_PARAMETERS/ {parameters_list}, yo
	
	! Открываем файл и считываем параметры
	!——————————————--
	open (11, file = 'parameters.data', delim = 'apostrophe')
	read(11, nml = NUM_METHOD_PARAMETERS)
	read (11, nml = MODEL_PARAMETERS)
	close (11)
	!——————————————--
	pp = [ {parameters_list} ]
	!——————————————--
	! Задаем начальные значения (близкие к положению равновесия)
	y = yo
	
	t = t_start
	!! Стохастическая модель Хищник-Жертва
	call StRKp3_d(Afunc, Bfunc, check, EQN, y, pp, t, t_start, t_stop, h)
end program main
'''
templates = {
	'functions' : Tf,
	'Afunc' : TA,
	'Bfunc' : TB,
	'main' : Tm,
}